package main

import (
	"fmt"
	"reflect"
)

type CSPReport struct {
	BlockedURI         string `json:"blocked-uri"`
	Disposition        string `json:"disposition"`
	DocumentURI        string `json:"document-uri"`
	EffectiveDirective string `json:"effective-directive"`
	OriginalPolicy     string `json:"original-policy"`
	Referrer           string `json:"referrer"`
	ScriptSample       string `json:"script-sample"`
	StatusCode         string `json:"status-code"`
	ViolatedDirective  string `json:"violated-directive"`
}

func (c CSPReport) String() string {
	val := reflect.ValueOf(c)
	out := ""
	for i := 0; i < val.NumField(); i++ {
		name := val.Type().Field(i).Name
		value := val.Type().Field(i)
		var pad string
		if i > 0 {
			pad = ", "
		}
		out += fmt.Sprintf("%s%s: %q", pad, name, val.FieldByIndex(value.Index))
	}

	return out
}
