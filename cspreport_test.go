package main

import (
	"fmt"
	"testing"
)

func TestCSPReportString(t *testing.T) {
	report := CSPReport{
		BlockedURI:         "A",
		Disposition:        "B",
		DocumentURI:        "C",
		EffectiveDirective: "D",
		OriginalPolicy:     "E",
		Referrer:           "F",
		ScriptSample:       "G",
		StatusCode:         "H",
		ViolatedDirective:  "I",
	}

	actual := report.String()
	expected := fmt.Sprintf("%s: %q, %s: %q, %s: %q, %s: %q, %s: %q, %s: %q, %s: %q, %s: %q, %s: %q", "BlockedURI", report.BlockedURI, "Disposition", report.Disposition, "DocumentURI", report.DocumentURI, "EffectiveDirective", report.EffectiveDirective, "OriginalPolicy", report.OriginalPolicy, "Referrer", report.Referrer, "ScriptSample", report.ScriptSample, "StatusCode", report.StatusCode, "ViolatedDirective", report.ViolatedDirective)

	if actual != expected {
		t.Errorf("expected: %q, actual: %q", expected, actual)
	}

}
