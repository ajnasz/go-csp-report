module gitlab.com/ajnasz/go-csp-report

replace github.com/Sirupsen/logrus => github.com/sirupsen/logrus v1.4.1

go 1.12

require (
	github.com/Sirupsen/logrus v1.4.2
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
)
