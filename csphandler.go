package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func cspHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var resp CSPJSON
	err = json.Unmarshal(body, &resp)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusAccepted)

	fmt.Println(resp.CSPReport.String())
}
