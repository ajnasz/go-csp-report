package main

import (
	"errors"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/coreos/go-systemd/activation"
	"github.com/coreos/go-systemd/daemon"
)

func logFatal(err error) {
	daemon.SdNotify(false, "STOPPING=1")
	fmt.Println(err)
	os.Exit(1)
}

func tick(tickChan chan interface{}, handler func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		tickChan <- struct{}{}
		handler(w, r)
	}
}

func main() {
	port := flag.String("port", ":12121", "The port where it should listen")
	timeout := (*flag.Int64("timeout", 5, "Number of seconds before timeout and exit"))
	flag.Parse()

	listeners, err := activation.Listeners()

	if err != nil {
		logFatal(err)
	}

	var l net.Listener

	if len(listeners) == 0 {
		l, err = net.Listen("tcp", *port)
		if err != nil {
			logFatal(err)
		}
	} else if len(listeners) != 1 {
		logFatal(errors.New("Unexpected number of socket activation fds"))
	} else {
		l = listeners[0]
	}

	defer l.Close()

	tickChan := make(chan interface{})
	http.HandleFunc("/", tick(tickChan, cspHandler))

	go func() {
		for {
			select {
			case <-tickChan:
			case <-time.After(time.Duration(int64(time.Second) * timeout)):
				daemon.SdNotify(false, "STOPPING=1")
				fmt.Println("Timeout, exiting...")
				os.Exit(0)
			}
		}
	}()

	if err != nil {
		logFatal(err)
	}

	daemon.SdNotify(false, "READY=1")
	fmt.Println(l.Addr())
	http.Serve(l, nil)

}
